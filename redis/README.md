How to use this ansible-playbook :

Define the clients on inventory/hosts_redis ,example I have redis-microservice
```
[redis-microservice]
redis-proxmox-test
[redis-proxmox-test]
redis-test	ansible_host=192.168.217.30
redis-test-2	ansible_host=192.168.217.31
redis-test-3	ansible_host=192.168.217.32
```
Then run the playbook for installation redis :

``ansible-playbook redis-installation.yml -i inventory/ -u [your_user] -Kkvvvvv -e 'HOSTS=redis-proxmox-test' -l redis-test,redis-test-2,redis-test-3``

How to use this ansible-playbook to set up replicaof (slave), first Install ansible module redis. 

``ansible-playbook redis-set-master-slave.yml -i inventory/ -u [your_username] -Kkvvvvv -e 'HOSTS=redis-proxmox-test' --tags "pythonmodule"``

on redis-set-master-slave.yml dont forget to define ip master on master_host

``ansible-playbook redis-set-master-slave.yml -i inventory/ -u [your_username] -Kkvvvvv -e 'HOSTS=redis-proxmox-test' -e 'MASTER_IP=[your_redis_master_ip]'``

Redis Sentinel

``ansible-playbook redis-sentinel.yml -i inventory/ -u [your_username] -Kkvvvvv -e 'HOSTS=redis-proxmox-test' -e 'SENTINEL_MASTER_IP=[your_redis_master_ip]'`` 
